package com.hamdaniekoprasetyo_10191036.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

        RecyclerView rv;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            String[] text = new String[]{
                    "hotel", "checkroom", "shuttle", "review"
            };

            int[] image = new int[]{
                    R.drawable.ic_baseline_hotel_24,
                    R.drawable.ic_baseline_checkroom_24,
                    R.drawable.ic_baseline_airport_shuttle_24,
                    R.drawable.ic_baseline_rate_review_24
            };


            rv = findViewById(R.id.recyclerView);
            Rvadapter adapter = new Rvadapter(this, text, image);
            rv.setAdapter(adapter);
            rv.setLayoutManager(new GridLayoutManager(this, 2));
    }
}